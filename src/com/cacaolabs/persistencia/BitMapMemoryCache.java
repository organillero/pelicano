package com.cacaolabs.persistencia;

import java.lang.ref.SoftReference;
import java.util.HashMap;

import android.graphics.Bitmap;

public class BitMapMemoryCache {
    private HashMap<String, SoftReference<Bitmap>> cache=new HashMap<String, SoftReference<Bitmap>>();
    
    public Bitmap get(String id){
        if(!this.cache.containsKey(id))
            return null;
        SoftReference<Bitmap> ref=this.cache.get(id);
        return ref.get();
    }
    
    public void put(String id, Bitmap bitmap){
        this.cache.put(id, new SoftReference<Bitmap>(bitmap));
    }

    public void clear() {
        this.cache.clear();
    }
}