package com.cacaolabs.persistencia;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;

public class FileSdStorage {

	private String filename;
	private FileOutputStream fos;
	private FileInputStream fis;

	public FileSdStorage (String fileName){
		this.filename=new String(fileName);
	}

	public Object loadObject(Context context){
		Object o = null;
		ObjectInputStream in;
		try {
			this.fis = context.openFileInput(this.filename);
			if(this.fis !=null){
				in = new ObjectInputStream(this.fis);
				o =  in.readObject();
				in.close();
			}
		} catch (Exception e) {}
		return o;
	}

	public void saveObject (Object o,Context context){
		ObjectOutputStream out;
		try {
			this.fos = context.openFileOutput(this.filename, Context.MODE_PRIVATE);
			out = new ObjectOutputStream(this.fos);
			out.writeObject(o);
			out.close();
		} catch (Exception e) {}
	}
}
