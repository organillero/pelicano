package com.cacaolabs.lacomer.interfaces;

public interface GetCategory {
	
	public String getCategory();

}
