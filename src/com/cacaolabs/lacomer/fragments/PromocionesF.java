package com.cacaolabs.lacomer.fragments;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.cacaolabs.lacomer.R;
import com.cacaolabs.lacomer.exceptions.OfflineException;
import com.cacaolabs.lacomer.interfaces.Update;
import com.cacaolabs.lacomer.pojos.Promocion;
import com.cacaolabs.lacomer.recursos.Recursos;
import com.cacaolabs.lacomer.recursos.Recursos.AnsType;
import com.cacaolabs.net.Net;
import com.cacaolabs.ui.UI;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class PromocionesF  extends SherlockListFragment implements Update{
	int mNum;
	//	private Sonido sonido;
	//	private final int  audio = R.raw.tab;
	private PromocionesAdapter t_adapter;

	private String name	= null;
	private boolean resume=false;
	private Activity context;
	private ProgressDialog m_ProgressDialog = null;
	private View viewer = null;
	private DisplayImageOptions options;
	private ImageLoader imageLoader = ImageLoader.getInstance();


	private String pasilloId;



	public PromocionesF (String pasilloId){

		this.pasilloId = pasilloId;
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {


		super.onCreate(savedInstanceState);

		setRetainInstance(true);
		this.context =getActivity();
		//		sonido = new Sonido(this.context);
		//		sonido.initSonido(this.audio);
		//		options = new DisplayImageOptions.Builder()
		//		.showStubImage(R.drawable.stub)
		//		.cacheInMemory()
		//		.cacheOnDisc()
		//		.build();
		//		imageLoader.init(ImageLoaderConfiguration.createDefault(context));


		if(savedInstanceState!=null){

			if (savedInstanceState.containsKey("pasilloId"))
				pasilloId = savedInstanceState.getString("pasilloId");
		}

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = this.context.getMenuInflater();
		
		int res;
		if (pasilloId != null)
			res = R.menu.menu_add_favoritos;
		else 
			res = R.menu.menu_edit_favoritos;

		
		inflater.inflate(res, menu);
		menu.setHeaderTitle(getString(R.string.opciones));

	}
	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (pasilloId != null ) outState.putSerializable("pasilloId", pasilloId);

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		this.viewer = inflater.inflate(R.layout.productos, container, false);

		if (getListAdapter() == null){
			this.t_adapter = new PromocionesAdapter(this.context, R.layout.promotion_item, new ArrayList<Promocion>());
			setListAdapter(this.t_adapter);
		}
		//		try {
		//			((Productos) this.context).showHeader();
		//		} catch (Exception e) {}
		updateData();
		return this.viewer;
	}

	@Override
	public void onResume() {
		super.onResume();
		this.resume =true;
	}

	@Override
	public void onPause() {
		super.onPause();
		this.resume=false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(this.name!=null)
			((MyFragmentActivity)this.context).removeDir("/"+this.name);
		imageLoader.stop();
	}


	private class PromocionesAdapter extends ArrayAdapter<Promocion> {
		private int textViewResourceId; 
		public PromocionesAdapter(Context context, int textViewResourceId, ArrayList<Promocion> arrayList ) {
			super(context , textViewResourceId, arrayList);
			this.textViewResourceId = textViewResourceId;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = LayoutInflater.from(PromocionesF.this.context);
				v = vi.inflate(this.textViewResourceId, null);
			}
			Promocion o = super.getItem(position);
			if (o != null){				
				((WebView) v.findViewById(R.id.webView1)).loadData(o.html, "text/html", "UTF-8");
				//				((TextView) v.findViewById(R.id.cat_number)).setText("(" + String.valueOf(o.total) + ")");
				//				if (o.imagen_url.equals("")== false){
				//					ImageView image=(ImageView)v.findViewById(R.id.product_image);
				//					image.setVisibility(View.VISIBLE);
				//					if(PromocionesF.this.resume==true){
				//						imageLoader.displayImage(o.imagen_url, image, options);
				//					}
				//				}
				//				else{
				//					ImageView image=(ImageView)v.findViewById(R.id.product_image);
				//					RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT);
				//					image.setLayoutParams(params);
				//				}
				//v.setBackgroundResource(R.drawable.selector_shape_fondo_list_item);
			}
			return v;
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		//TODO
		//		Pasillo o = (Pasillo) getListAdapter().getItem(position);
		//		if (o != null){
		//			clear();
		//			//sonido.play(false);
		//			Fragment fragmnet = (o.final_ == false)   ? new CategoriasF(o.id) : new ProductosF(o.id);
		//			if(o.final_==false){
		//				((CategoriasF)fragmnet).setName(o.name);
		//			}
		//			else{
		//				((ProductosF)fragmnet).setName(o.name);
		//			}
		//			( (MyFragmentActivity) this.context).addDir(o.name);
		//			( (MyFragmentActivity) this.context).startNextFragment(fragmnet);
		//		}
	}

	@Override
	public void updateData(){
		new Thread(null, this.runGetCategories, "getCategories").start();
		this.m_ProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.espere), getString(R.string.obteniendo), true,true,
				new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				( (MyFragmentActivity) PromocionesF.this.context).removeFragment();
			}
		});
		clear();
	}

	public void setName(String name){
		this.name=new String(name);
	}

	private void clear(){
		this.t_adapter.clear();
		this.t_adapter.notifyDataSetChanged();
	}

	/*
	 *Runnables para ejecutar los webServices getCategories y getProducts 
	 *
	 */
	private Runnable runGetCategories = new Runnable(){
		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			AnsType ansType = AnsType.UNKNOW;
			Map<String, List<Promocion>> promociones = null;
			try {
				String url;

				if (pasilloId == null){
					url = Recursos.URL_SERVER + "ls_promo_persona.php?user_id=1" ;

				}
				else {
					url = Recursos.URL_SERVER + "ls_promo_todo.php" ;
				}





				Type fooType = (Type) new TypeToken<Map<String, List<Promocion>>>() {}.getType();

				promociones =  (Map<String, List<Promocion>>) Net.sendDataAndGetObjectCache(url, fooType, null);
				ansType = AnsType.OK;
			}  catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
			}
			PromocionesF.this.context.runOnUiThread(new ResGetCategories(promociones,ansType));
		}
	};

	class  ResGetCategories implements Runnable {

		Map<String, List<Promocion>> promociones;
		AnsType ansType;
		public ResGetCategories (Map<String, List<Promocion>> promociones, AnsType ansType){
			if ( promociones != null){
				this.promociones = promociones; 

			} 
			this.ansType=ansType;
		}
		@Override
		public void run() {
			PromocionesF.this.m_ProgressDialog.dismiss();
			clear();
			switch (this.ansType) {
			case OK:
				if(this.promociones != null){
					if ( this.promociones.containsKey(pasilloId)){
						for( Promocion promocion : this.promociones.get(pasilloId))
							PromocionesF.this.t_adapter.add(promocion);
					}
					else {
						for (Map.Entry<String,List<Promocion>> entry : promociones.entrySet()) {

							for( Promocion promocion : this.promociones.get(entry.getKey()))
								PromocionesF.this.t_adapter.add(promocion);


						}


					}

				}
				else{

					( (MyFragmentActivity) PromocionesF.this.context).removeFragment();
					UI.showAlertDialog("Upps!", "No se encontraron promociones en este pasillo.", getString(R.string.ok), PromocionesF.this.context, null).show();
				}
				PromocionesF.this.t_adapter.notifyDataSetChanged();
				break;
			case OFFLINE:
				if(PromocionesF.this.resume==true)
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_sin_internet), getString(R.string.ok), PromocionesF.this.context, null).create().show();
				( (MyFragmentActivity) PromocionesF.this.context).removeFragment();
				break;
			case NETWORK_PROBLEM:
				if(PromocionesF.this.resume==true)
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok), PromocionesF.this.context, null).create().show();
				( (MyFragmentActivity) PromocionesF.this.context).removeFragment();
				break;
			case UNKNOW:
			case DATA_PROBLEM:
			default:
				if(PromocionesF.this.resume==true)
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_datos_corruptos), getString(R.string.ok), PromocionesF.this.context, null).create().show();
				( (MyFragmentActivity) PromocionesF.this.context).removeFragment();
				break;
			}
		}
	}


}
