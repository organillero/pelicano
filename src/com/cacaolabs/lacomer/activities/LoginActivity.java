package com.cacaolabs.lacomer.activities;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.facebook.FacebookActivity;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;



public class LoginActivity extends FacebookActivity {

    private SharedPreferences mSharedPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        if (isSessionOpen()) {
            Toast.makeText(this, "Hi" + getSession().getAccessToken() + "!",
                    Toast.LENGTH_LONG).show();
        } else {
            this.openSession();
        }
        finish();
    }

    @Override
    protected void onSessionStateChange(SessionState state, Exception exception) {
        // user has either logged in or not ...
        if (state.isOpened()) {
            // make request to the /me API
            Request request = Request.newMeRequest(this.getSession(),
                    new Request.GraphUserCallback() {
                        // callback after Graph API response with user object
                        @Override
                        public void onCompleted(GraphUser user,
                                Response response) {
                            if (user != null) {
                                storePreferences(user);
                                Toast.makeText(LoginActivity.this,
                                        "Hello " + user.getName() + "!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            Request.executeBatchAsync(request);
        } else {
            storePreferences(null);
        }
    }

    public boolean storePreferences(GraphUser user) {
        Editor editor = mSharedPref.edit();

        if (user == null) {
            editor.clear();
            return editor.commit();
        }

        editor.putString("expiration_date_key", getSession()
                .getExpirationDate().toString());
        editor.putString("access_token_key", getSession().getAccessToken());

        editor.putString("facebook_username_key", user.getUsername());
        editor.putString("facebook_first_name_key", user.getFirstName());
        editor.putString("facebook_middle_name_key", user.getMiddleName());
        editor.putString("facebook_last_name_key", user.getLastName());
        editor.putString("facebook_birthday_key", user.getBirthday());

        if (user.getLocation() == null) {
            editor.remove("facebook_latitude_key");
            editor.remove("facebook_longitude_key");
            editor.remove("facebook_street_key");
            editor.remove("facebook_city_key");
            editor.remove("facebook_state_key");
            editor.remove("facebook_country_key");
            editor.remove("facebook_zipcode_key");
        } else {
            editor.putFloat("facebook_latitude_key", (float) user.getLocation()
                    .getLatitude());
            editor.putFloat("facebook_longitude_key", (float) user
                    .getLocation().getLongitude());
            editor.putString("facebook_street_key", user.getLocation()
                    .getStreet());
            editor.putString("facebook_city_key", user.getLocation().getCity());
            editor.putString("facebook_state_key", user.getLocation()
                    .getState());
            editor.putString("facebook_country_key", user.getLocation()
                    .getCountry());
            editor.putString("facebook_zipcode_key", user.getLocation()
                    .getZip());
        }
        return editor.commit();
    }
}
