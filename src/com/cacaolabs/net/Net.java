package com.cacaolabs.net;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.cacaolabs.lacomer.App;
import com.cacaolabs.lacomer.exceptions.OfflineException;
import com.cacaolabs.persistencia.FileCache;
import com.cacaolabs.persistencia.ObjectMemoryCache;
import com.cacaolabs.utils.Utils;
import com.google.gson.GsonBuilder;

public class Net {

	private static Context context;
	private static FileCache fileCache;
	private static ObjectMemoryCache objMemoryCache;

	static{
		context = App.getInstance().getApplicationContext();
		fileCache=new FileCache(context);
		objMemoryCache =  App.getInstance().getStringMemoryCache();
	}


	public  static Object sendDataAndGetObjectCache (String url, Type type, List<NameValuePair> nameValuePairs) throws Exception{
		Object o=null;
		String shaUrl=Utils.getSHA1(url);
		//memoria cache
		o =  objMemoryCache.get(shaUrl);
		if (o != null)
			return o;
		//archivo cache
		File f=fileCache.getFile(shaUrl);
		// && new FileInputStream(f).available()>0
		if (f.exists()) {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
			o =  in.readObject();
			in.close();
		}
		else{ 
			//web
			if (isOnline() == false )
				throw new OfflineException();
			o = sendDataAndGetObject(url, type, nameValuePairs);
			if (o != null){
				ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
				out.writeObject(o);
				out.close();
			}
		}
		objMemoryCache.put(shaUrl, o);
		return o;
	}

	public static Object sendDataAndGetObject  (String url,  Type type, List<NameValuePair> nameValuePairs) throws OfflineException,  IOException, Exception {
		Object o = null;
		InputStream is = sendData( url, nameValuePairs);
		if (is != null && is instanceof InputStream){
			String ansUrl = inputStreamToString (is);
			try {
				o = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create().
				fromJson(ansUrl, type);
			} catch (Exception e) {
				o=null;
			}finally{
				ansUrl=null;
				is=null;
				System.gc();
			}
		}
		return o;
	}

	public static Object sendDataAndGetObjectZip  (String url,  Type type, List<NameValuePair> nameValuePairs) throws OfflineException,  IOException, Exception {
		Object o = null;
		InputStream is = sendData( url, nameValuePairs);
		if (is != null && is instanceof InputStream){
			String ansUrl = "";
			//ZipEntry ze = null;
			ZipInputStream zin =new  ZipInputStream(is);
			while (( zin.getNextEntry()) != null) { 
				for (int c = zin.read(); c != -1; c = zin.read()) { 
					char car = (char) c;
					ansUrl=ansUrl.concat(""+car);
				}

			} 
			zin.close();
			try {
				o = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create().
				fromJson(ansUrl, type);
			} catch (Exception e) {
				o=null;
			}
		}
		return o;
	}

	public static InputStream sendData(String url, List<NameValuePair> nameValuePairs) throws  OfflineException, IOException, Exception {
		if (isOnline()== false){
			throw new OfflineException();
		}
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
		HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
		HttpRequestBase httpRequest;
		//POST 
		if (nameValuePairs != null){
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			for(int index=0; index < nameValuePairs.size(); index++) {
				if(nameValuePairs.get(index).getName().equalsIgnoreCase("image")) {
					entity.addPart(nameValuePairs.get(index).getName(), new FileBody(new File (nameValuePairs.get(index).getValue())));
				} else {
					entity.addPart(nameValuePairs.get(index).getName(), new StringBody(nameValuePairs.get(index).getValue()));
				}
			}
			httpRequest = new HttpPost(url);
			((HttpPost) httpRequest).setEntity(entity);
		}
		//GET
		else{
			httpRequest = new HttpGet(url);
		}

		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpResponse response = httpClient.execute(httpRequest);
		return  response.getEntity().getContent();
	}

	public static String addParameter(String url, String name, String value)
	{
		if  (name != null && value != null){
			int qpos = url.indexOf('?');
			int hpos = url.indexOf('#');
			char sep = qpos == -1 ? '?' : '&';
			String seg = sep + encodeUrl(name) + '=' + encodeUrl(value);
			return hpos == -1 ? url + seg : url.substring(0, hpos) + seg+ url.substring(hpos);
		}
		return url;
	}

	public  static boolean isOnline() {		
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean ans;
		final NetworkInfo network_info = cm.getActiveNetworkInfo();
		ans = network_info != null && network_info.isConnected() ? true : false; 
		return ans;
	}

	public static String inputStreamToString (InputStream in)  {	

		String res=null;
		//		StringWriter writer = new StringWriter();
		//		
		//		try {
		//			IOUtils.copy(in, writer, "UTF-8");
		//			 res = writer.toString();
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//			return null;
		//		}




		//		 	java.util.Scanner s = new java.util.Scanner(in, "UTF-8").useDelimiter("\\A");
		//    		res =  s.hasNext() ? s.next() : "";
		//
		//    		in = null;
		//    		s = null;
		//    		System.gc();

		//		try {
		//			res = CharStreams.toString(new InputStreamReader(in, Charsets.UTF_8));
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}



		StringBuilder out = new StringBuilder();
		if (in == null) return null;
		byte[] b = new byte[8192];
		try {
			for (int n; (n = in.read(b)) != -1;) {
				out.append(new String(b, 0, n));
			}
			res=out.toString();
			System.gc();
		} catch (IOException e) {
			e.printStackTrace();
		}catch(Exception e){
			System.gc();
			//Log.e("Test", "Error de memoria");
			return null;
		}
		System.gc();

		return res;

	}

	public static String convertStreamToString(InputStream is) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		is.close();
		return sb.toString();
	}

	private static String encodeUrl(String url)	{
		try{
			return URLEncoder.encode(url, "UTF-8");		}
		catch (UnsupportedEncodingException uee){
			throw new IllegalArgumentException(uee);	}
	}

//	public static UrlShortener getUrlShort(String url){
//		UrlShortener o=null;
//		if (url == null)
//			return null;
//		try {
//			DefaultHttpClient httpclient = new DefaultHttpClient();
//			HttpPost httppost = new HttpPost(Recursos.URL_SERVER_SHORTENER);
//			httppost.setHeader("Content-type", "application/json");
//			httppost.setEntity(new StringEntity("{\"longUrl\": \"" + url + "\"}"));
//			HttpResponse response = httpclient.execute(httppost);
//			InputStream out = response.getEntity().getContent();
//			if ((out) instanceof InputStream){
//				String ansUrl = inputStreamToString (out);
//				o = new Gson().fromJson(ansUrl, UrlShortener.class);
//			}
//		} catch (Exception e) {
//			o = null;
//		}
//		return o;
//	}

	public  static Boolean checkObjectCache(String url) throws Exception{
		Object o=null;
		o =  objMemoryCache.get(Utils.getSHA1(url));
		if (o != null)
			return true;
		File f=fileCache.getFile(Utils.getSHA1(url));
		if (f.exists()) {
			return true;
		}else
			return false;
	}
}
