package com.cacaolabs.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

public class UI {
	
	public static void showToast(String message, Context context){
		Toast.makeText(context,message, Toast.LENGTH_LONG).show();	
	}
	
	public static AlertDialog.Builder showAlertDialog(String title, String message,String button, Context context, final Cancel o ){
		AlertDialog.Builder builder = new AlertDialog.Builder( context );
		builder	.setTitle(title)
				.setMessage(message)
				.setCancelable(false)
				.setNeutralButton(button, new DialogInterface.OnClickListener() {
		           @Override
				public void onClick(DialogInterface dialog, int id) {
		        	   dialog.cancel();
		        	   if (o != null) o.cancel();
		           }
		       });

		return builder; 
	}

}
